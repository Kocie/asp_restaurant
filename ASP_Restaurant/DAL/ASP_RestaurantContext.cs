﻿using ASP_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP_Restaurant.DAL
{
    public class ASP_RestaurantContext : DbContext
    {
        public ASP_RestaurantContext() : base("ASP_Restaurant")
        {

        }

        public DbSet<UserAdmin> UserAdmins { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<CustomerOrder> CustomerOrders { get; set; }

        public DbSet<OrderedProduct> Orderedproducts { get; set; }

        public DbSet<Cart> Carts { get; set; }
    }
}
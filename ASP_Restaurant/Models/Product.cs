﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASP_Restaurant.Models
{
    public class Product
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Nazwa dania jest wymagana")]
        [MaxLength(45, ErrorMessage = "Maksymalna nazwa nie może być dłuższa niż 45 liter")]
        [Display(Name = "Nazwa")]
        public string Name { get; set; }

        // [DataType(DataType.Currency)]
        // [Required(ErrorMessage = "Price is required")]
        // [Range(typeof(decimal), "0.01", "100000.00", ErrorMessage = "enter decimal value")]
        //  [RegularExpression(@"^[0-9]{1,6}.[0-9]{2}$", ErrorMessage = "enter decimal value of format $9.99")]
        // public decimal Price { get; set; }
        //[RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Has to be decimal with two decimal points")]
        //[Range(0, 50, ErrorMessage = "The maximum possible value should be upto 5 digits")]
        [Display(Name = "Cena")]
        public Decimal Price { get; set; }

        [Display(Name = "Opis")]
        public string Description { get; set; }

        [Display(Name = "Zaktualizowane")]
        [Column(TypeName = "datetime2")]
        public DateTime LastUpdated { get; set; }


        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<OrderedProduct> OrderedProducts { get; set; }

    }
}
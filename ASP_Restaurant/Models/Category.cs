﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_Restaurant.Models
{
    public class Category
    {
        public int Id { get; set; }

        [Display(Name = "Kategoria")]
        [Required(ErrorMessage = "Nazwa kategorii jest wymagana")]
        [MaxLength(45, ErrorMessage = "Maksymalnie 45 liter")]
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
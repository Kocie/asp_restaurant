﻿using ASP_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_Restaurant.ViewModel
{
    public class ShoppingCartViewModel
    {
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}
﻿using ASP_Restaurant.DAL;
using ASP_Restaurant.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ASP_Restaurant.Controllers
{
    public class UserAdminController : Controller
    {
        private ASP_RestaurantContext db = new ASP_RestaurantContext();


        // GET: Products
        public ActionResult Index()
        {
            return View(db.UserAdmins.ToList());
        }

        // GET: UserAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        
        public ActionResult Create([Bind(Include = "Id,UserName,Email,Password,ConfirmPassword")] UserAdmin useradmin)
        {
           
            if (ModelState.IsValid)
            {
                db.UserAdmins.Add(useradmin);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            return View(useradmin);
        }

        //public ActionResult LogOff()
        //{

        //}

        public ActionResult Login()
        {
            return View();
        }



        [HttpPost]
       
        public ActionResult Login(LoginModel userAdmin)
        {

            var userExists = db.UserAdmins.SingleOrDefault(x => x.UserName == userAdmin.UserName);

            if (userExists != null)
            {
                Session["loginSuccess"] = true;
                Session["user"] = userAdmin.UserName;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
            ////The ".FirstOrDefault()" method will return either the first matched
            ////result or null
            //var myUser = db.UserAdmins
            //.FirstOrDefault(u => u.UserName == userAdmin.UserName
            //             && u.Password == userAdmin.Password);

            //if (myUser != null)    //User was found
            //{
            //    FormsAuthentication.SetAuthCookie(userAdmin.UserName, userAdmin.Remember);
            //    //Session.Add("username", HttpContext.Current.User.Identity.Name);
            //    //Session.Remove("username");
            //    //Session["UserName"] = userAdmin.UserName;
            //    //Session["UserId"] = userAdmin.UserName;
            //    return RedirectToAction("Index", "Home");
            //}
            //else    //User was not found
            //{
            //    return View();
            //}
        }

        public ActionResult Logout()
        {
            Session["loginSuccess"] = null;
            Session.Abandon();

            return RedirectToAction("Index", "Home");
            
        }



        public ActionResult Dashboard(LoginModel user)
        {
            if (Session["user"] == null)
            {
                return RedirectToAction("Login", "UserAdmin");
            }
            string username = Session["user"].ToString();
            UserAdmin currentUser = db.UserAdmins.Where(x => x.UserName == username).FirstOrDefault();
            ViewBag.User = currentUser;

            return RedirectToAction("Dashboard", "UserAdmin");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}